const rollupUtils = require('rollup-pluginutils');
const babel = require('@babel/core');
const { generate } = require('astring')

export default function(options = {}) {
    options = Object.assign({}, options);
    const filter = rollupUtils.createFilter(
        options.include || [
            '**/*.{js,mjs,jsx}',
        ],
        options.exclude || [
            /node_modules\/@babel\/runtime\/helpers/,
        ],
    );

    delete options.include;
    delete options.exclude;

    return {
        name: 'babel',

        async transform(code, id) {
            if (!filter(id)) return null;
            const { ast } = await babel.transformAsync(code, Object.assign({
                filename: id,
            }, options, {
                ast: true,
                code: false,
                sourceMap: false,
                parserOpts: {
                    plugins: ['estree'],
                },
                presets: options.presets || [],
                plugins: options.plugins || [],
            }));

            return {
                code: generate(ast.program),
                ast,
            };
        },
    };
};
