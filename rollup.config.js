import babel from './plugins/rollup-babel-ast-plugin/index';

export default {
    input: 'src/index.js',

    output: {
        file: 'dist/index.js',
        format: 'umd',
    },

    plugins: [
        babel(),
    ],
};