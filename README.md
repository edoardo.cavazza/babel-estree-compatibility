# Babel ESTree Compatibility

A test project for Babel compatibility with ESTree spec. The aim of this project is to find *every* incompatibility between Babel and ESTree specs used by other tools such as `astring` and [`rollup`](https://rollupjs.org/), in order to [speed up builds](https://rollupjs.org/guide/en/#transform).

## Install

```sh
npm install
```

## Build

```sh
npm run build
```

## Issues

* Transpiling JSX:
    * Property value of ObjectProperty expected node to be of a type ["Expression","PatternLike"] but instead got "Literal"
